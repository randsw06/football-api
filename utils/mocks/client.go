// Package mocks for mock public API response
package mocks

import "net/http"

// MockClient is mock for public API
type MockClient struct {
	DoFunc func(req *http.Request) (*http.Response, error)
}

// Do used for mock public API request
func (m *MockClient) Do(req *http.Request) (*http.Response, error) {
	return m.DoFunc(req)
}
