// Package dbrequest used for communication with PostgreSQL database
package dbrequest

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	backoff "github.com/cenkalti/backoff/v4"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	footballApi "github.com/randsw/football-api/football-api"
	"github.com/randsw/football-api/logger"
	"go.uber.org/zap"
)

func createTable(p *pgxpool.Pool) error {
	_, err := p.Exec(context.Background(),
		"CREATE TABLE IF NOT EXISTS fixtures(id SERIAL PRIMARY KEY, Date DATE, Home VARCHAR(64), Away VARCHAR(64), HomeGoals INT, AwayGoals INT, Status VARCHAR(64));")
	if err != nil {
		return err
	}
	return nil
}

// DBConnect used for connect ot PostgreSQL database
func DBConnect() *pgxpool.Pool {
	// Initialize ENV
	// Get Postgres host
	pgHost := "127.0.0.1"
	if envvar := os.Getenv("PG_HOST"); len(envvar) > 0 {
		pgHost = envvar
	}
	// Get Postres port
	pgPort := "5432"
	if envvar := os.Getenv("PG_PORT"); len(envvar) > 0 {
		pgPort = envvar
	}
	pgPortInt, err := strconv.Atoi(pgPort)
	if err != nil {
		// handle error
		logger.Error("PG port number not valid", zap.String("err", err.Error()))
		os.Exit(2)
	}
	// Get Postgres User
	pgUser := "fixtures"
	if envvar := os.Getenv("PG_USER"); len(envvar) > 0 {
		pgUser = envvar
	}
	// Get Postgres password
	pgPass := "football"
	if envvar := os.Getenv("PG_PASSWORD"); len(envvar) > 0 {
		pgPass = envvar
	}
	// Get Postgres database name
	pgDbname := "fixtures"
	if envvar := os.Getenv("PG_DBNAME"); len(envvar) > 0 {
		pgDbname = envvar
	}
	// Create DB connect string
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", pgHost, pgPortInt, pgUser, pgPass, pgDbname)
	var conn *pgxpool.Pool
	operation := func() error {
		conn, err = pgxpool.Connect(context.Background(), psqlconn)
		if err != nil {
			logger.Error("Unable to connect to database", zap.String("err", err.Error()))
			return err
		}
		return nil
	}
	err = backoff.Retry(operation, backoff.NewExponentialBackOff())
	logger.Info("Connect to postgreSQL base", zap.String("Host", pgHost), zap.String("Port", strconv.Itoa(pgPortInt)))
	err = createTable(conn)
	{
		if err != nil {
			logger.Error("Cannot create table fixtures", zap.String("err", err.Error()))
			os.Exit(1)
		}
	}
	return conn
}

// Select is used to get data from PostgreSQL database
func Select(p *pgxpool.Pool, date time.Time) (*footballApi.FormatedResult, error) {
	var err error
	conn, err := p.Acquire(context.Background())
	if err != nil {
		return nil, err
	}
	defer conn.Release()

	rows, err := conn.Query(context.Background(), "SELECT Date, Home, Away, HomeGoals, AwayGoals, Status FROM fixtures WHERE Date = $1", date)
	if err == pgx.ErrNoRows {
		return nil, err
	}
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var rec footballApi.FormatedResult
	for rows.Next() {
		var f footballApi.Fixture
		if err := rows.Scan(&f.Date, &f.HomeTeam, &f.AwayTeam, &f.HomeTeamGoals, &f.AwayTeamGoals, &f.GameStatus); err != nil {
			return nil, err
		}
		rec.Fixtures = append(rec.Fixtures, f)
	}
	return &rec, nil
}

// Insert is used to write data from PostgreSQL database
func Insert(p *pgxpool.Pool, apiResult *footballApi.FormatedResult) {
	var err error
	conn, err := p.Acquire(context.Background())
	if err != nil {
		logger.Error("Unable to acquire a database connection", zap.String("err", err.Error()))
		return
	}
	defer conn.Release()

	// Delete all entry for current date. Then insert
	var f footballApi.Fixture
	row := conn.QueryRow(context.Background(),
		"SELECT * FROM fixtures WHERE Date = $1", apiResult.Fixtures[0].Date)
	if err = row.Scan(&f.HomeTeam, &f.AwayTeam); err != pgx.ErrNoRows {
		// Delete all rows with this date
		_, err = conn.Exec(context.Background(), "DELETE FROM fixtures WHERE Date = $1", apiResult.Fixtures[0].Date)
		if err != nil {
			logger.Error("Unable to DELETE", zap.String("err", err.Error()))
			return
		}
	}

	for _, fixture := range apiResult.Fixtures {
		row := conn.QueryRow(context.Background(),
			"INSERT INTO fixtures (Date, Home, Away, HomeGoals, AwayGoals, Status) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id",
			fixture.Date, fixture.HomeTeam, fixture.AwayTeam, fixture.HomeTeamGoals, fixture.AwayTeamGoals, fixture.GameStatus)
		var id uint64
		err = row.Scan(&id)
		if err != nil {
			logger.Error("Unable to INSERT", zap.String("err", err.Error()))
			return
		}
	}
}
