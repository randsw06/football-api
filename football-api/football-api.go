// Package footballapi used to parse data from API and write to PostgreSQL database
package footballapi

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	jsonstruct "github.com/randsw/football-api/JSONstruct"
	apierrors "github.com/randsw/football-api/football-api-errors"
	"github.com/randsw/football-api/logger"
	"go.uber.org/zap"
)

// QueryParams is parameters to request to public API
type QueryParams struct {
	date     string
	leagueID string
	season   string
}

// FormatedResult is human-readeble format data for football matchs
type FormatedResult struct {
	Fixtures []Fixture
}

// Fixture is human-readeble format data for football match
type Fixture struct {
	Date          time.Time
	HomeTeam      string
	AwayTeam      string
	HomeTeamGoals int
	AwayTeamGoals int
	GameStatus    string
}

type httpClient interface {
	Do(req *http.Request) (*http.Response, error)
}

var (
	client httpClient
)

func init() {
	client = &http.Client{}
}

func apiRequest(params QueryParams) ([]byte, error) {
	var err error
	// Initialize ENV
	// Get pod namespace
	apiURL := "https://v3.football.api-sports.io/fixtures"
	if envvar := os.Getenv("API_URL"); len(envvar) > 0 {
		apiURL = envvar
	}
	// Get HTTP method name
	method := "GET"
	if envvar := os.Getenv("API_METHOD"); len(envvar) > 0 {
		method = envvar
	}
	// Get footbal API key
	apiKey := "examplekey"
	if envvar := os.Getenv("API_KEY"); len(envvar) > 0 {
		apiKey = envvar
	}

	req, err := http.NewRequest(method, apiURL, nil)
	if err != nil {
		logger.Error("Cannot create new http requsest", zap.String("err", err.Error()))
		return nil, err
	}

	q := req.URL.Query()
	q.Add("date", params.date)
	q.Add("league", params.leagueID) // 39 - England Premier league
	q.Add("season", params.season)
	req.URL.RawQuery = q.Encode()

	req.Header.Add("x-apisports-key", apiKey)

	res, err := client.Do(req)
	if err != nil {
		logger.Error("Fail to execute API request", zap.String("err", err.Error()))
		return nil, err
	}
	logger.Info("Execute API request", zap.String("requestURL", req.URL.String()))
	defer func() {
		err = res.Body.Close()
		if err != nil {
			log.Printf("can't close http response: %v", err)
		}
	}()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		logger.Error("Fail to extract response body", zap.String("err", err.Error()))
		return nil, err
	}

	return body, nil
}

// GetResult reutrn formatted response form public API
func GetResult(params QueryParams) (*FormatedResult, error) {
	response, err := apiRequest(params)
	if err != nil {
		return nil, err
	}
	responseJSON, err := getJSON(response)
	if err != nil {
		return nil, err
	}
	switch value := responseJSON.Errors.(type) {
	case map[string]interface{}:
		var errorString string
		for k, v := range value {
			errorString = k + ": " + v.(string)
		}
		customError := apierrors.NewError(errorString)
		return nil, customError
	default:
		logger.Info("Got some strange error from API")
	}
	formatResult := getFormatResult(responseJSON)
	return formatResult, err
}

func getFormatResult(apiREsponse *jsonstruct.APIResponse) *FormatedResult {
	result := FormatedResult{}
	dateValue, err := time.Parse("2006-01-02", apiREsponse.Parameters.Date)
	if err != nil {
		return nil
	}
	for _, fixture := range apiREsponse.Response {
		result.Fixtures = append(result.Fixtures, Fixture{Date: dateValue, HomeTeam: fixture.Teams.Home.Name, AwayTeam: fixture.Teams.Away.Name,
			HomeTeamGoals: fixture.Goals.Home, AwayTeamGoals: fixture.Goals.Away,
			GameStatus: fixture.Fixture.Status.Short})
	}
	return &result
}

func getJSON(body []byte) (*jsonstruct.APIResponse, error) {
	response := &jsonstruct.APIResponse{}
	err := json.Unmarshal(body, &response)
	if err != nil {
		logger.Error("Fail to umarshal response body", zap.String("err", err.Error()))
		return nil, err
	}
	return response, nil
}

// NewQueryParams return new QueryParams struct
func NewQueryParams(date string, leagueID string, season string) QueryParams {
	return QueryParams{date: date, leagueID: leagueID, season: season}
}
