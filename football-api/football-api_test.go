package footballapi

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"
	"testing"

	"github.com/randsw/football-api/logger"
	"github.com/randsw/football-api/utils/mocks"
)

func TestGetFixedValue(t *testing.T) {
	logger.InitLogger()
	client = &mocks.MockClient{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			if req.URL.Path != "/fixtures" {
				t.Errorf("Expected to request '/fixtures', got: %s", req.URL.Path)
			}
			if req.Header.Get("x-apisports-key") == "" {
				t.Errorf("Expected x-apisports-key header")
			}
			if len(req.URL.Query()) != 3 {
				t.Errorf("Expected 3 query parameters, got: %d", len(req.URL.Query()))
			}
			responseBody := io.NopCloser(bytes.NewReader(SetTestResponse()))
			return &http.Response{
				StatusCode: 200,
				Body:       responseBody,
			}, nil
		},
	}
	params := QueryParams{date: "2022-10-30", leagueID: "39", season: "2022"}

	result, _ := apiRequest(params)

	responseJSON, _ := getJSON(result)

	if len(responseJSON.Response) != 2 {
		t.Errorf("Expected 2 fixture, got: %d", len(responseJSON.Response))
	}
	t.Log("API response is correct")
}

func TestGetFormatResult(t *testing.T) {
	res, _ := getJSON(SetTestResponse())
	formatedResult := getFormatResult(res)
	expectedResult := `[{"Date":"2022-10-30T00:00:00Z","HomeTeam":"Arsenal","AwayTeam":"Nottingham Forest","HomeTeamGoals":5,"AwayTeamGoals":0,"GameStatus":"FT"},{"Date":"2022-10-30T00:00:00Z","HomeTeam":"Manchester United","AwayTeam":"West Ham","HomeTeamGoals":1,"AwayTeamGoals":0,"GameStatus":"FT"}]`
	formatedResultString, err := json.Marshal(formatedResult.Fixtures)
	if err != nil {
		t.Errorf("Function output is not proper JSON")
	}
	if len(formatedResultString) != len([]rune(expectedResult)) {
		t.Errorf("Output\n %s\n not equal to expected\n %s\n", formatedResultString, []byte(expectedResult))
		return
	}
	for i := range formatedResultString {
		if formatedResultString[i] != []byte(expectedResult)[i] {
			t.Errorf("Output\n %s\n not equal to expected\n %s\n", formatedResultString, expectedResult)
			return
		}
	}
	t.Log("Response is correct")
}

func TestGetJsonCount(t *testing.T) {
	expectedFixtures := 2
	response, _ := getJSON(SetTestResponse())
	if len(response.Response) != expectedFixtures {
		t.Errorf("Output %q not equal to expected %q", len(response.Response), expectedFixtures)
		return
	}
	t.Log("Number of fixture is correct")
}

func TestGetErrorJsonCount(t *testing.T) {
	logger.InitLogger()
	_, err := getJSON(SetErrorTestResponse())
	if err == nil {
		t.Errorf("Expected a error, got nothing")
		return
	}
	t.Logf("Got a error %s", err.Error())
}

func TestGetJsonTeams(t *testing.T) {
	expectedHomeTeam := "Manchester United"
	expectedAwayTeam := "West Ham"
	response, _ := getJSON(SetTestResponse())
	if response.Response[1].Teams.Home.Name != expectedHomeTeam || response.Response[1].Teams.Away.Name != expectedAwayTeam {
		t.Errorf("Output %s and %s are not equal to expected %s and %s",
			response.Response[1].Teams.Home.Name, response.Response[1].Teams.Away.Name, expectedHomeTeam, expectedAwayTeam)
		return
	}
	t.Log("Teams names is correct")
}

func TestGetJsonScores(t *testing.T) {
	expectedHomeGoals := 1
	expectedAwayGoals := 0
	response, _ := getJSON(SetTestResponse())
	if response.Response[1].Goals.Home != expectedHomeGoals || response.Response[1].Goals.Away != expectedAwayGoals {
		t.Errorf("Output %q and %q are not equal to expected %q and %q", response.Response[1].Goals.Home,
			response.Response[1].Goals.Away, expectedHomeGoals, expectedAwayGoals)
		return
	}
	t.Log("Match score is correct")
}

func SetTestResponse() []byte {
	test := `{"get":"fixtures","parameters":{"date":"2022-10-30","league":"39","season":"2022"},"errors":{},"results":2,"paging":{"current":1,"total":1},"response":[{"fixture":{"id":868077,"referee":"Simon Hooper, England","timezone":"UTC","date":"2022-10-30T14:00:00+00:00","timestamp":1667138400,"periods":{"first":1667138400,"second":1667142000},"venue":{"id":494,"name":"Emirates Stadium","city":"London"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https://media.api-sports.io/football/leagues/39.png","flag":"https://media.api-sports.io/flags/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":42,"name":"Arsenal","logo":"https://media.api-sports.io/football/teams/42.png","winner":true},"away":{"id":65,"name":"Nottingham Forest","logo":"https://media.api-sports.io/football/teams/65.png","winner":false}},"goals":{"home":5,"away":0},"score":{"halftime":{"home":1,"away":0},"fulltime":{"home":5,"away":0},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868084,"referee":"Chris Kavanagh, England","timezone":"UTC","date":"2022-10-30T16:15:00+00:00","timestamp":1667146500,"periods":{"first":1667146500,"second":1667150100},"venue":{"id":556,"name":"Old Trafford","city":"Manchester"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https://media.api-sports.io/football/leagues/39.png","flag":"https://media.api-sports.io/flags/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":33,"name":"Manchester United","logo":"https://media.api-sports.io/football/teams/33.png","winner":true},"away":{"id":48,"name":"West Ham","logo":"https://media.api-sports.io/football/teams/48.png","winner":false}},"goals":{"home":1,"away":0},"score":{"halftime":{"home":1,"away":0},"fulltime":{"home":1,"away":0},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}}]}`
	return []byte(test)
}

func SetErrorTestResponse() []byte {
	test := `{{{{"get":"fixtures","parameters":{"date":"2022-10-29","league":"39","season":"2022"},"errors":{},"results":8,"paging":{"current":1,"total":1},"response":[{"fixture":{"id":868076,"referee":"Anthony Taylor, England","timezone":"UTC","date":"2022-10-29T14:00:00+00:00","timestamp":1667052000,"periods":{"first":1667052000,"second":1667055600},"venue":{"id":504,"name":"Vitality Stadium","city":"Bournemouth, Dorset"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":35,"name":"Bournemouth","logo":"https:\/\/media.api-sports.io\/football\/teams\/35.png","winner":false},"away":{"id":47,"name":"Tottenham","logo":"https:\/\/media.api-sports.io\/football\/teams\/47.png","winner":true}},"goals":{"home":2,"away":3},"score":{"halftime":{"home":1,"away":0},"fulltime":{"home":2,"away":3},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868078,"referee":"Robert Madley, England","timezone":"UTC","date":"2022-10-29T14:00:00+00:00","timestamp":1667052000,"periods":{"first":1667052000,"second":1667055600},"venue":{"id":10503,"name":"Gtech Community Stadium","city":"Brentford, Middlesex"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":55,"name":"Brentford","logo":"https:\/\/media.api-sports.io\/football\/teams\/55.png","winner":null},"away":{"id":39,"name":"Wolves","logo":"https:\/\/media.api-sports.io\/football\/teams\/39.png","winner":null}},"goals":{"home":1,"away":1},"score":{"halftime":{"home":0,"away":0},"fulltime":{"home":1,"away":1},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868079,"referee":"Andy Madley, England","timezone":"UTC","date":"2022-10-29T14:00:00+00:00","timestamp":1667052000,"periods":{"first":1667052000,"second":1667055600},"venue":{"id":508,"name":"The American Express Community Stadium","city":"Falmer, East Sussex"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":51,"name":"Brighton","logo":"https:\/\/media.api-sports.io\/football\/teams\/51.png","winner":true},"away":{"id":49,"name":"Chelsea","logo":"https:\/\/media.api-sports.io\/football\/teams\/49.png","winner":false}},"goals":{"home":4,"away":1},"score":{"halftime":{"home":3,"away":0},"fulltime":{"home":4,"away":1},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868080,"referee":"Salisbury Michael, England","timezone":"UTC","date":"2022-10-29T14:00:00+00:00","timestamp":1667052000,"periods":{"first":1667052000,"second":1667055600},"venue":{"id":525,"name":"Selhurst Park","city":"London"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":52,"name":"Crystal Palace","logo":"https:\/\/media.api-sports.io\/football\/teams\/52.png","winner":true},"away":{"id":41,"name":"Southampton","logo":"https:\/\/media.api-sports.io\/football\/teams\/41.png","winner":false}},"goals":{"home":1,"away":0},"score":{"halftime":{"home":1,"away":0},"fulltime":{"home":1,"away":0},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868081,"referee":"John Brooks, England","timezone":"UTC","date":"2022-10-29T16:30:00+00:00","timestamp":1667061000,"periods":{"first":1667061000,"second":1667064600},"venue":{"id":535,"name":"Craven Cottage","city":"London"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":36,"name":"Fulham","logo":"https:\/\/media.api-sports.io\/football\/teams\/36.png","winner":null},"away":{"id":45,"name":"Everton","logo":"https:\/\/media.api-sports.io\/football\/teams\/45.png","winner":null}},"goals":{"home":0,"away":0},"score":{"halftime":{"home":0,"away":0},"fulltime":{"home":0,"away":0},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868082,"referee":"Robert Jones, England","timezone":"UTC","date":"2022-10-29T11:30:00+00:00","timestamp":1667043000,"periods":{"first":1667043000,"second":1667046600},"venue":{"id":547,"name":"King Power Stadium","city":"Leicester, Leicestershire"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":46,"name":"Leicester","logo":"https:\/\/media.api-sports.io\/football\/teams\/46.png","winner":false},"away":{"id":50,"name":"Manchester City","logo":"https:\/\/media.api-sports.io\/football\/teams\/50.png","winner":true}},"goals":{"home":0,"away":1},"score":{"halftime":{"home":0,"away":0},"fulltime":{"home":0,"away":1},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868083,"referee":"Michael Oliver, England","timezone":"UTC","date":"2022-10-29T18:45:00+00:00","timestamp":1667069100,"periods":{"first":1667069100,"second":1667072700},"venue":{"id":550,"name":"Anfield","city":"Liverpool"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":40,"name":"Liverpool","logo":"https:\/\/media.api-sports.io\/football\/teams\/40.png","winner":false},"away":{"id":63,"name":"Leeds","logo":"https:\/\/media.api-sports.io\/football\/teams\/63.png","winner":true}},"goals":{"home":1,"away":2},"score":{"halftime":{"home":1,"away":1},"fulltime":{"home":1,"away":2},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}},{"fixture":{"id":868085,"referee":"Paul Tierney, England","timezone":"UTC","date":"2022-10-29T14:00:00+00:00","timestamp":1667052000,"periods":{"first":1667052000,"second":1667055600},"venue":{"id":562,"name":"St. James' Park","city":"Newcastle upon Tyne"},"status":{"long":"Match Finished","short":"FT","elapsed":90}},"league":{"id":39,"name":"Premier League","country":"England","logo":"https:\/\/media.api-sports.io\/football\/leagues\/39.png","flag":"https:\/\/media.api-sports.io\/flags\/gb.svg","season":2022,"round":"Regular Season - 14"},"teams":{"home":{"id":34,"name":"Newcastle","logo":"https:\/\/media.api-sports.io\/football\/teams\/34.png","winner":true},"away":{"id":66,"name":"Aston Villa","logo":"https:\/\/media.api-sports.io\/football\/teams\/66.png","winner":false}},"goals":{"home":4,"away":0},"score":{"halftime":{"home":1,"away":0},"fulltime":{"home":4,"away":0},"extratime":{"home":null,"away":null},"penalty":{"home":null,"away":null}}}]}`
	return []byte(test)
}
