// Package jsonstruct used to create Go struct from JSON API response
package jsonstruct

// APIResponse is global response from API
type APIResponse struct {
	// Request type
	Get string `json:"get"`
	// Request parameters
	Parameters Parameters  `json:"parameters"`
	Errors     interface{} `json:"errors"` // map[string]string
	// Number of fixtures
	Result int `json:"results"`
	// Response page number
	Paging Paging `json:"paging"`
	// Main response
	Response []Fixture `json:"response"`
}

// Parameters contains request data for API
type Parameters struct {
	// Date
	Date string `json:"Date"`
	// League ID
	League string `json:"league"`
	// Season year
	Season string `json:"season"`
}

// Paging is pagination information from response
type Paging struct {
	// Current page number
	Current int `json:"current"`
	// Total page numbers
	Total int `json:"total"`
}

// Fixture held information about fixture from response
type Fixture struct {
	Fixture Matches `json:"fixture"`
	League  League  `json:"league"`
	Teams   Teams   `json:"teams"`
	Goals   Goals   `json:"goals"`
	Scores  Scores  `json:"score"`
}

// Matches held information about match from response
type Matches struct {
	// Fixture ID
	ID int `json:"id"`
	// Referee name
	Referee string `json:"referee"`
	// Timezone
	Timezone string `json:"timezone"`
	// Date
	Date string `json:"datetime"`
	// Timestamp
	Timestamp int `json:"timestamp"`
	// Periods
	Periods Periods `json:"periods"`
	// Venue
	Venue Venue `json:"venue"`
	// Status
	Status Status `json:"status"`
}

// League held information about league from response
type League struct {
	// League ID
	ID int `json:"id"`
	// League name
	Name string `json:"name"`
	// League country
	Country string `json:"country"`
	// League logo
	Logo string `json:"logo"`
	// League flag
	Flag string `json:"flag"`
	// League Season
	Season int `json:"season"`
	// League Round
	Round string `json:"round"`
}

// Teams held information about teams from response
type Teams struct {
	// Home team
	Home Team `json:"home"`
	// Away team
	Away Team `json:"away"`
}

// Goals held information about goals in match from response
type Goals struct {
	// Home goals
	Home int `json:"home,omitempty"`
	// Away goals
	Away int `json:"away,omitempty"`
}

// Scores held information about scores in match from response
type Scores struct {
	// Halftime goals
	Halftime Goals `json:"halftime"`
	// Fulltime goals
	Fulltime Goals `json:"fulltime"`
	// Extratime goals
	Extratime Goals `json:"extratime"`
	// Penalty goals
	Penalty Goals `json:"penalty"`
}

// Team held information about team play in match from response
type Team struct {
	// Team ID
	ID int `json:"id"`
	// Team name
	Name string `json:"name"`
	// Team country
	Logo string `json:"logo"`
	// Winner
	Winner bool `json:"winner"`
}

// Periods held information start time of first and second half in UNIX epochs
type Periods struct {
	// Periods start time in UNIX epochs
	First  int `json:"first"`
	Second int `json:"second"`
}

// Venue held information about match stadium from response
type Venue struct {
	// Venue information
	ID int `json:"id"`
	// Stadium name
	Name string `json:"name"`
	// City name
	City string `json:"city"`
}

// Status held information about match status from response
type Status struct {
	// Long version of match status
	Long string `json:"long"`
	// Short version of match status
	Short string `json:"short"`
	// Match time
	Elapsed int `json:"elapsed"`
}
