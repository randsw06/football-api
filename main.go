package main

import (
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	dbrequest "github.com/randsw/football-api/db-request"
	footballApi "github.com/randsw/football-api/football-api"
	"github.com/randsw/football-api/logger"
	prom "github.com/randsw/football-api/prometheus-exporter"
	"go.uber.org/zap"
)

//Global var, used in status display
var (
	tag  string
	hash string
	date string
)

func errorWrongParams(w http.ResponseWriter, requestQueryParams url.Values) {
	w.WriteHeader(400)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Parameters number not equal 3. Got " + strconv.Itoa(len(requestQueryParams))
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		logger.Error("Error while marshaling wrong parameters num", zap.String("err", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		logger.Error("Can't send http packet", zap.String("err", err.Error()))
	}
}

func errorAPI(w http.ResponseWriter, err error) {
	w.WriteHeader(400)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Error while requesting football API: " + err.Error()
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		logger.Error("Error while marshaling request football API", zap.String("err", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		logger.Error("Can't send http packet", zap.String("err", err.Error()))
	}
}

func getAPI(conn *pgxpool.Pool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		logger.Info("Receive API request", zap.String("requestURL", r.URL.String()))
		// Get Query parameters
		requestQueryParams := r.URL.Query()
		if len(requestQueryParams) != 3 {
			logger.Error("Receive parameters not equal 3", zap.Int("Recevie parameters number", len(requestQueryParams)))
			errorWrongParams(w, requestQueryParams)
			return
		}
		// Fill parameters struct
		params := footballApi.NewQueryParams(requestQueryParams["date"][0], requestQueryParams["league"][0], requestQueryParams["season"][0])
		// Get formated result
		score, err := footballApi.GetResult(params)
		if err != nil {
			logger.Error("Error while requesting football API", zap.String("err", err.Error()))
			errorAPI(w, err)
			return
		}
		// Write data to DB
		dbrequest.Insert(conn, score)
		// Create response
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(score.Fixtures)
		if err != nil {
			logger.Error("Error while encoding JSON response", zap.String("err", err.Error()))
		}
	}
}

func errorDateFormat(w http.ResponseWriter) {
	w.WriteHeader(400)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Date wrong format. Waiting for YYYY-MM-DD"
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		logger.Error("Error while marshaling wrong format response", zap.String("err", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		logger.Error("Can't send http packet", zap.String("err", err.Error()))
	}
}

func errorNoFixtures(w http.ResponseWriter) {
	w.WriteHeader(404)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Fixture not found for this date"
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		logger.Error("Error while marshaling not found response", zap.String("err", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		logger.Error("Can't send http packet", zap.String("err", err.Error()))
	}
}

func errorDBFail(w http.ResponseWriter) {
	w.WriteHeader(500)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Internal server error"
	jsonResp, err := json.Marshal(resp)
	if err != nil {
		logger.Error("Error while marshaling internal server error", zap.String("err", err.Error()))
	}
	_, err = w.Write(jsonResp)
	if err != nil {
		logger.Error("Can't send http packet", zap.String("err", err.Error()))
	}
}

func getDB(conn *pgxpool.Pool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		// Get Query parameters
		logger.Info("Receive API request", zap.String("requestURL", r.URL.String()))
		requestQueryParams := r.URL.Query()
		dateValue, err := time.Parse("2006-01-02", requestQueryParams["date"][0])
		if err != nil {
			logger.Error("Date in wrong format", zap.String("err", err.Error()))
			errorDateFormat(w)
			return
		}
		score, err := dbrequest.Select(conn, dateValue)
		if len(score.Fixtures) == 0 {
			logger.Error("Fixture not found", zap.String("date", dateValue.String()))
			errorNoFixtures(w)
			return
		} else if err != nil {
			logger.Error("Unable to SELECT", zap.String("err", err.Error()))
			errorDBFail(w)
			return
		}
		// Create response
		w.Header().Set("Content-Type", "application/json")
		err = json.NewEncoder(w).Encode(score.Fixtures)
		if err != nil {
			logger.Error("Error while encoding JSON response", zap.String("err", err.Error()))
		}
	}
}

func getHealth(w http.ResponseWriter, r *http.Request) {
	enc := json.NewEncoder(w)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	resp := map[string]string{
		"name":   "Football API",
		"status": "OK",
		"tag":    tag,
		"hash":   hash,
		"date":   date,
	}
	if err := enc.Encode(resp); err != nil {
		logger.Error("Error while encoding JSON response", zap.String("err", err.Error()))
	}
}

func metrics(w http.ResponseWriter, r *http.Request) {
	promhttp.Handler().ServeHTTP(w, r)
}

func main() {
	// Loger Initialization
	logger.InitLogger()
	defer logger.CloseLogger()
	conn := dbrequest.DBConnect()
	defer conn.Close()
	// Initialize HTTP server
	mux := mux.NewRouter()
	mux.Use(prom.PrometheusMiddleware)
	mux.HandleFunc("/get-api", getAPI(conn))
	mux.HandleFunc("/get-db", getDB(conn))
	mux.HandleFunc("/healthz", getHealth)
	mux.HandleFunc("/metrics", metrics)
	servingAddress := ":8080"
	srv := &http.Server{
		Addr: servingAddress,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      mux, // Pass our instance of gorilla/mux in.
	}
	logger.Info("Start serving http request...", zap.String("address", servingAddress))
	err := srv.ListenAndServe()
	if err != nil {
		logger.Error("Fail to start http server", zap.String("err", err.Error()))
	}
}
