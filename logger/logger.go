// Package logger implements wrap on ZapLogger
package logger

import (
	"log"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type zaplog struct {
	logger *zap.Logger
}

var zaplogger zaplog

// InitLogger used for initialize logger
func InitLogger() {
	var err error
	cfg := zap.NewProductionConfig()
	cfg.EncoderConfig.EncodeTime = zapcore.RFC3339TimeEncoder
	cfg.EncoderConfig.FunctionKey = "func"
	zaplogger.logger, err = cfg.Build(zap.AddCallerSkip(1))
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}
}

// CloseLogger is used for sync logger in application closing stage
func CloseLogger() {
	err := zaplogger.logger.Sync()
	if err != nil {
		log.Printf("can't sync zap logger: %v", err)
	}
}

// Info is wrap for Zaplogger info method
func Info(message string, fields ...zap.Field) {
	zaplogger.logger.Info(message, fields...)
}

// Warn is wrap for Zaplogger warning method
func Warn(message string, fields ...zap.Field) {
	zaplogger.logger.Warn(message, fields...)
}

// Debug is wrap for Zaplogger debug method
func Debug(message string, fields ...zap.Field) {
	zaplogger.logger.Debug(message, fields...)
}

// Error is wrap for Zaplogger error method
func Error(message string, fields ...zap.Field) {
	zaplogger.logger.Error(message, fields...)
}

// Fatal is wrap for Zaplogger fatal method
func Fatal(message string, fields ...zap.Field) {
	zaplogger.logger.Fatal(message, fields...)
}
