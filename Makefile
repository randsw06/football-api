GO_VERSION := 1.21

TAG := $(shell git describe --abbrev=0 --tags --always)
HASH := $(shell git rev-parse HEAD)
DATE := $(shell date +%Y-%m-%d.%H:%M:%S)

LDFLAGS := -w -X main.hash=$(HASH) \
			-X main.tag=$(TAG) \
			-X main.date=$(DATE)

build:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags "$(LDFLAGS)" -a -o football-api-app main.go 