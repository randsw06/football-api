# Footbal API

![Pipeline](https://gitlab.com/randsw06/football-api/badges/main/pipeline.svg)

[![coverage report](https://gitlab.com/randsw06/football-api/badges/main/coverage.svg)](https://gitlab.com/randsw06/football-api/-/commits/main)

![Release](https://gitlab.com/randsw06/football-api/-/badges/release.svg?order_by=release_at)

## Description

> :warning: This is education project, not for production. Used to master technologies for use in air-gapped infrastructure

Get English Premier League scores by date using <https://www.api-football.com/> and write parsed result to PostgreSQL DB.

## Needs

- Kubernetes cluster
- PostreSQL database

## Deploy

Get API key from <https://www.api-football.com/>

Create kubernetes secret with your API key

```bash
kubectl create secret generic footbal-api-key \
    --from-literal=API_KEY=<your-api-key> \
    -n football-api
```

Create kubernetes secret with your database password

```bash
kubectl create secret generic pg-password \
    --from-literal=PG_PASSWORD=<your-database-password> \
    -n football-api
```

Create `values--deploy.yaml` file

Add following content to file

```yaml
env:
  enabled: true
  envs:
    API_URL: "https://v3.football.api-sports.io/fixtures"
    PG_HOST: <your-posgreSQL-host>
    PG_PORT: <your-posgreSQL-port>
    PG_USER: <your-posgreSQL-user>
    PG_DBNAME: <your-posgreSQL-databasename>
```

Deploy application using helm chart to kubernetes cluster

```bash
helm repo add football-api https://gitlab.com/api/v4/projects/40752011/packages/helm/stable
helm upgrade --install --create-namespace football-api/football-api -f values-deploy.yaml -n footlball-api
```

By default football-api ingress exposed at NodePort 32080 on all nodes

Add to `/etc/host`

```bash
<ip-address-of-any-node-in-cluster> api.local
```

Example request:
Get all fixtures from EPL at 23 september 2023:

```bash
curl "api.local:32080/get-api?date=2023-09-23&league=39&season=2023"
```

After previous request you can get information from database not asking the API

```bash
curl "api.local:32080/get-db?date=2023-09-23&league=39&season=2023"
```
